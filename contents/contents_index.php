<?php
    if($connected == true)
    {
        list($nick, ,$email,$admin) = explode(";;",$_COOKIE['INTEGRADOR_USER_CONNECTED_COOKIE']);
        
        $preview = false;
        
        if($admin)
        {
            if(!empty($_POST))
            {
                if($_POST['buttonPress']=='submit')
                    require_once('php/get_article.php');	//LÓGICA DE OBTENCION DEL ARTICULO
                    
                elseif($_POST['buttonPress']=='preview')
                {
                    require_once('php/preview_article.php');
                    
                    $preview = true;
                }
            }
            
        if($preview == false)    
            require_once('html/html_article_form.html');
            
        }
    }
    						
    $cnx = mysqli_connect('localhost', DB_USR, DB_PASS, 'fit2014');

    if($cnx)
    {
            $query = select_all_from('article');	//$query = 'SELECT * FROM `comment`';
            
            $resultado = mysqli_query( $cnx, $query );
            
            if($resultado)	echo getArticles($resultado);
            
            else echo "Error al realizar el query";
    }
    
    else echo "Error al conectarse a la base de datos";


?>


<!--

<h3>¿Qué es el FIT Joven?</h3>

<p>Es una acción conjunta del Ministerio de Desarrollo Social y la Dirección General de Políticas de Juventud del Gobierno de la Ciudad de Buenos Aires que tiene por objeto mejorar las oportunidades de inserción en el mercado de trabajo a través del otorgamiento de becas de capacitación para una serie de cursos cortos dictados por instituciones de trayectoria en diferentes áreas temáticas. 
Está destinado a jóvenes residentes en la Ciudad Autónoma de Buenos Aires que se encuentren en situación de desempleo u ocupación informal.</p>

<br/>
<br/>

<h3>Cursos de capacitación</h3>

<p>La meta principal de los cursos de capacitación es brindar herramientas a los jóvenes participantes para su inserción en el mercado de trabajo. Tienen una duración de 4 meses (de mediados de agosto a mediados de diciembre) y se cursan una vez por semana durante dos, tres o cuatro horas. Los cursos se vinculan con la demanda del mercado laboral actual y son dictados por instituciones educativas reconocidas y de amplia trayectoria.</p>

<br/>
<br/>

<h3>Oferta de cursos, instituciones, días y horarios</h3>

<p><img src="http://www.buenosaires.gob.ar/sites/gcaba/files/cuadro-cursos-fit_1_0.jpg" width="600" height="818" alt="" title="" /></p>

<br/>
<br/>

<h3>Espacio de tutorías</h3>

<p>Como complemento de esta acción en el espacio de tutorías se trabaja acompañando a cada uno de los jóvenes en su proceso de inserción laboral. Mediante la modalidad de taller mensual se brinda capacitación y formación para la búsqueda laboral a través de la reflexión grupal en torno a las características del mercado actual y sus diferentes formas de inserción. Se trabajan temáticas tales como: autoconocimiento y perfil laboral, estrategias para la búsqueda de empleo, armado de CV, entrevista laboral y autoempleo.</p>

<br/>

<h4>Requisitos de admisión</h4>

<ul>
<li>Ser mayor de 18 años de edad (se requerirá la presentación DNI que lo acredite).</li>
<li>Tener domicilio en CABA, acreditando una residencia mínima, inmediata e interrumpida de 2 años (se requerirá la presentación de DNI que lo acredite)</li>
<li>Encontrarse en situación de desempleo u ocupación informal (se requerirá la presentación del certificación negativa expedido por ANSES que lo acredite)</li>
</ul>

<br/>

<h4>Importante</h4>

<p>Las vacantes son limitadas. En atención al tipo de cursos que en esta oportunidad se ofrecen, tendrán prioridad los postulantes que cuenten con secundario completo y una formación afín al curso elegido.</p>

<br/>
<br/>

<h3>Convocatoria:</h3>

<p>Postulate entre el <strong>21 y el 31 de julio</strong> completando el siguiente <a href="https://docs.google.com/forms/d/1HoIwQtjAQfHHHp-WGbYcaxxlCkZGLkaPF1Xrd4TbnXU/viewform">formulario</a></p>
-->