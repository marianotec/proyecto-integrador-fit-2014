<?php
	/** ************** LOGICA DE CONEXIÓN Y REGISTRO ************** **/
	$connected  = false;
	
	if(ISSET($_POST))
	{		
		require_once('php/register_logic.php');

		require_once('php/login_assert.php');
	}
	/** ************** ************** ************** ************** **/
	
	if( isset( $_COOKIE['INTEGRADOR_USER_CONNECTED_COOKIE'] ) )
	{
		setcookie('INTEGRADOR_USER_CONNECTED_COOKIE', NULL , time()-1000 );
		
		header('Location: login.php');
	}
	
	if($connected == false)
	{
		$nick = '';
		
		$passwd = '';
		
		$remember = '';
		
		if( isset( $_COOKIE['INTEGRADOR_USER_REMEMBER_COOKIE'] ) )
		{
			list($nick, $passwd, $email) = explode (";;",$_COOKIE['INTEGRADOR_USER_REMEMBER_COOKIE']);
			
			$remember = 'checked';
		}
		
		require_once('html/html_show_login.html');
	}
	
	else	header('Location: index.php');
?>