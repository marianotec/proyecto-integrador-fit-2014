<?php	
	
	if(ISSET($_COOKIE['INTEGRADOR_USER_CONNECTED_COOKIE']))
	{
		list($nick,$email) = explode (";;",$_COOKIE['INTEGRADOR_USER_CONNECTED_COOKIE']);
		$buttons = array
				(	//Asociación (name)		//Contenido(link)
					'Inicio'			=>	'index.php', 
					'Listado'			=>	'list.php',
					'Libro de Visitas'	=>	'book.php', 
					'Contacto'			=>	'contact.php', 
					"$nick - Cerrar Session"
										=>	'login.php'
				);
	}
	
	else
	{
		$buttons = array
					(	//Asociación (name)		//Contenido(link)
						'Inicio'			=>	'index.php', 
						//'Listado'			=>	'list.php',
						'Libro de Visitas'	=>	'book.php', 
						'Contacto'			=>	'contact.php', 
						'Ingresar'			=>	'login.php'
					);
	}
	
	echo '<ul>';
	
	foreach( $buttons as $button_name=>$link )
	{
		$css = '';
		if( $actualPage == $link ) $css = 'active';
		
		echo "
			<li class='$css'>
				<a href=$link>
					$button_name
				</a>
			</li>\r\n
			";
	}	
	
	echo '</ul>';
?>