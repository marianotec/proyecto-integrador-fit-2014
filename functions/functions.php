<?php
	function getPage()
	{	
		return ( pathinfo( ( $_SERVER['PHP_SELF'] ), PATHINFO_BASENAME ) );
	}
	
	// ---------------------------------------------------------------------------------
	
	function select_all_from($table)
	{
		return "SELECT * FROM `$table`";
	}
	
	// ---------------------------------------------------------------------------------
	
	function select_all_from_where($table, $arg)
	{
		return "SELECT * FROM `$table` WHERE $arg";
	}
	
	// ---------------------------------------------------------------------------------
	
	function getComments($result)
	{
	
		$commentsHTML = '';
							
		$mail = '';
		
		while( $comment = mysqli_fetch_assoc( $result ) )
		{																
			if(!empty($comment['mail']))	$mail = "($comment[mail])";
			
			else	$mail = '';
			
			$commentsHTML ="
							<div style='text-align:left;'>
								<br/>
								<h3>$comment[nick] $mail</h3>
								<br/>
								<p>$comment[message]</p>
								<br/>
								<h5>$comment[time]</h5>
								<br/>
								<hr/>
								<!-- <hr/>: pone una linea \"separadora\" que ocupa todo el ancho del bloque -->
							</div>
							".$commentsHTML;
		}
		
		return $commentsHTML;
	}
	
	// ---------------------------------------------------------------------------------
	
	function getArticles($result)
	{
	
		$articlesHTML = '';
							
		$author = '';
		
		while( $article = mysqli_fetch_assoc( $result ) )
		{
			
			$articlesHTML ="
							<div style='text-align:left;'>
								<br/>
								<h1>$article[title]</h1>
								<br/>
								<p>$article[content]</p>
								<br/>
								<br/>
								<p><strong>Tags: $article[tags]</strong></p>
								<br/>
								<h5 style='text-align:right'>Posted by $article[author] on $article[date]</h5>
								<br/>
								<hr/>
								<!-- <hr/>: pone una linea \"separadora\" que ocupa todo el ancho del bloque -->
							</div>
							".$articlesHTML;
		}
		
		return $articlesHTML;
	}
	
?>