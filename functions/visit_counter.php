<?php
	$archivo = 'contador.txt';
	
	//
	if( isset($_GET['reset'] ) ) unlink($archivo);
	
	//defino una variable que comienza en 0
	$n = 0;
	if( file_exists($archivo) ){
		//pero si existe el archivo la variable vale lo que esté en el archivo
		
		//al decir (int) fuerzo a que el valor a asignar sea un Integer
		$n = (int)  file_get_contents($archivo);
	}	
	
	$n++;
	//si el archivo no existe lo crea y si existe lo pisa!
	file_put_contents( $archivo, $n );
	
	echo $n;
?>