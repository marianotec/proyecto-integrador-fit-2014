<SCRIPT LANGUAGE="JavaScript">
	
	function wichButton(form,button_name)
	{
		form.buttonPress.value = button_name;
	}

</script>

<?php
    $article = '';
    
    $title = '';
    
    $tags = '';
    
    $author = '';
    
    $date = '';

   	if(!empty($_POST["content"]) && !empty($_POST["title"]))
    {
        $article = $_POST["content"]; // === (isset($_POST["comment"]) &&  ($_POST["comment"] != NULL);
        
        $title =  $_POST["title"];
        
        $tags = '';
        
        date_default_timezone_set("America/Argentina/Buenos_Aires");
        
        $time = date("d/m/Y H:i:s");
        
        if(!empty($_POST["tags"]))  $tags = $_POST["tags"];
        
        list($author) = explode(";;",$_COOKIE['INTEGRADOR_USER_CONNECTED_COOKIE']);
        
        $preview = "
                    <div style='text-align:left;'>
                        <hr style='border-color: red;'/>
                        <center><h1 style='color: red;'>Vista previa</h1></center>
                        <br/><br/>
                        <h1>$title</h1>
                        <br/>
                        <p>$article</p>
                        <br/>
                        <p><strong>Tags: $tags</strong></p>
                        <br/>
                        <h5>Posted by $author on $time</h5>
                        <br/><br/>
                        <center><h1 style='color: red;'>Fin vista previa</h1></center>
                        <hr style='border-color: red;'/>
                        <!-- <hr/>: pone una linea \"separadora\" que ocupa todo el ancho del bloque -->
                    </div>
                    ";
    }

?>

<html>
    <form action='index.php' method='post'>
        <fieldset>
                
            <legend><h3>Publica un nuevo artículo.</h3></legend>
            
            <br/>
    
            <table style='width: 100%;'>
                
                <tr>
                    <td>
                        <input type='text' name='title' id='title' value='<?php echo $title; ?>' style='height: 30px; width: 100%; resize: none;' required/>
                    </td>
                </tr>
    
                <tr>
                    <td>
                        <textarea name='content' id='content' style='height: 190px; width: 100%; resize: none;' required><?php echo $article; ?></textarea>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <input type='text' maxlength="100" name='tags' id='tags' value='<?php echo $tags; ?>' style='height: 30px; width: 100%; resize: none;'/>
                    </td>
                </tr>
    
                <tr>
                    <td>
                        <input type='submit' name='publicar' id='publicar' value='Publicar' onclick="wichButton(this.form,'submit')"/>
                        
                        <input type='submit' name='preview' id='preview' value='Vista Previa' onclick="wichButton(this.form,'preview')"/>
                        
                        <input type="hidden" name="buttonPress">
                    </td>
                </tr>
            
            </table>
            
        </fieldset>
    </form>
    
    <br/>
    <br/>
    
    <?php echo $preview; ?>
    
</html>